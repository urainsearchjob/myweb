FROM python:3.9 as build-env
ENV VIRTUAL_ENV /env
ENV PATH /env/bin:$PATH
ADD . /code
WORKDIR /code
RUN pip install -r requirements.txt
# CMD python app.py

FROM python:3.9-alpine
ADD . /myapp
WORKDIR /myapp
COPY --from=build-env . /myapp
RUN python -m venv env
RUN pip install -r requirements.txt 
#CMD ["docker run -it myweb /bin/bash"]
#CMD ["python", "app.py", "makemigrations"]
#CMD ["python", "app.py", "migrate"] 
 #CMD ["python", "app.py", "runserver", "0.0.0.0:5000"] 
CMD python app.py
